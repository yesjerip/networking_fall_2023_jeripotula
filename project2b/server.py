import socket
import select
import json
import sys

# Function to send a message to a client
def send_message(client_socket, message):
    try:
        json_message = json.dumps(message)
        client_socket.sendall(json_message.encode('utf-8'))
    except Exception as e:
        print("Error sending message:", str(e))
        return False
    return True

# Function to broadcast a message to all clients except the sender
def broadcast_message(sender_socket, clients, message):
    for client_socket in clients:
        if client_socket != server_socket and client_socket != sender_socket:
            send_message(client_socket, message)

# Function to handle a client's connect message
def handle_connect(client_socket, message, clients, subscriptions):
    user_name = message.get("user_name", "")
    targets = message.get("targets", [])

    if not user_name.startswith("@") or len(user_name) > 60:
        send_message(client_socket, {"status": "error", "message": "Invalid user name"})
        return

    for target in targets:
        if not (target.startswith("#") and len(target) <= 60):
            send_message(client_socket, {"status": "error", "message": f"Invalid target: {target}"})
            return

    subscriptions[client_socket] = {"user_name": user_name, "targets": targets}
    send_message(client_socket, {"status": "connected"})

# Function to handle a client's disconnect message


def handle_disconnect(client_socket, clients, subscriptions):
    user_name = subscriptions[client_socket]["user_name"]
    broadcast_message(None, clients, {"status": "chat", "history": [{"from": "@server", "message": f"{user_name} has disconnected."}]})
    del subscriptions[client_socket]
    clients.remove(client_socket)
    print(f"Client {user_name} disconnected")

#def handle_disconnect(client_socket, clients, subscriptions):
 #   user_name = subscriptions.get(client_socket, {}).get("user_name", "")
  #  clients.remove(client_socket)

   # try:
    #    del subscriptions[client_socket]
    #except KeyError:
     #   pass  # Handle the case where the client is not in the subscriptions dictionary

    #print(f"Client {user_name} disconnected")
    broadcast_message(None, clients, {"status": "chat", "history": [{"from": "@server", "message": f"{user_name} has disconnected."}]})

# Function to handle a client's message
def handle_message(client_socket, message, clients, subscriptions, message_queue):
    user_name = subscriptions[client_socket]["user_name"]
    target = message.get("target", "")
    message_text = message.get("message", "")

    if not (target.startswith("#") or target.startswith("@")):
        send_message(client_socket, {"status": "error", "message": "Invalid target"})
        return

    if len(message_text) > 3800:
        send_message(client_socket, {"status": "error", "message": "Message too long"})
        return

    message_queue.append({"from": user_name, "target": target, "message": message_text})

# Function to handle errors
def handle_error(client_socket, message):
    send_message(client_socket, {"status": "error", "message": message})

# Function to handle incoming messages from clients
def handle_client_message(client_socket, clients, subscriptions, message_queue):
    try:
        data = client_socket.recv(4096).decode('utf-8')
        if not data:
            handle_disconnect(client_socket, clients, subscriptions)
            return

        message = json.loads(data)
        action = message.get("action", "")

        if action == "connect":
            handle_connect(client_socket, message, clients, subscriptions)
        elif action == "disconnect":
            handle_disconnect(client_socket, clients, subscriptions)
        elif action == "message":
            handle_message(client_socket, message, clients, subscriptions, message_queue)
        else:
            handle_error(client_socket, "Invalid action")
    except json.JSONDecodeError:
        handle_error(client_socket, "Malformed JSON")
    except UnicodeDecodeError:
        handle_error(client_socket, "Malformed UTF-8 Data")
    except Exception as e:
        handle_error(client_socket, str(e))

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 server.py <ip/hostname> <port>")
        sys.exit(1)

    server_host = sys.argv[1]
    server_port = int(sys.argv[2])

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((server_host, server_port))
    server_socket.listen()

    clients = [server_socket]
    subscriptions = {}
    message_queue = []

    print(f"Server listening on {server_host}:{server_port}")

    while True:
        readable, _, _ = select.select(clients, [], [])

        for sock in readable:
            if sock == server_socket:
                # New connection
                client_socket, addr = server_socket.accept()
                clients.append(client_socket)
                print(f"New connection from {addr}")
            else:
                # Handle client message
                handle_client_message(sock, clients, subscriptions, message_queue)

        # Send queued messages to clients
        while message_queue:
            message = message_queue.pop(0)
            target = message["target"]

            for client_socket, subscription in subscriptions.items():
                if target in subscription["targets"] or target == subscription["user_name"]:
                    send_message(client_socket, {"status": "chat", "history": [message]})
