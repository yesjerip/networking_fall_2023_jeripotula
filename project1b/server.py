import socket
import threading
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
import time
import struct

# Constants
SERVER_IP = "127.0.0.1" #CHANGE THIS TO YOUR IP ADDRESS TO CONNECT 
SERVER_PORT = 8000
INVALID_MSG = "Invalid message format"

# Game constants
EMPTY = 0
X_PLAYER = 1
O_PLAYER = 2

class TicTacToeServer:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((ip, port))
        self.clients = {}
        self.games = {}
        self.locks = defaultdict(threading.Lock)

    def start(self):
        print(f"Server started on {self.ip}:{self.port}")
        executor = ThreadPoolExecutor(max_workers=10)
        while True:
            data, address = self.socket.recvfrom(65535)
            executor.submit(self.handle_client, data, address)
            self.check_timeouts()

    def handle_client(self, data, address):
        # Unpack message
        #game_id, msg_id, flags, state = struct.unpack("!IHB18s", data[:25])
        game_id, msg_id, flags, state = struct.unpack("!IBH18s", data[:25])  # Corrected 'H' to 'B'

        msg = data[25:].decode("utf-8")

        with self.locks[game_id]:
            if game_id not in self.games:
                # Validate and start a new game
                if msg_id or flags or int.from_bytes(state, 'big'):
                    self.send_error(address, "Invalid new game")
                    return
                self.games[game_id] = Game()
                self.clients[address] = game_id
                print(f"{address} started a new game {game_id}")

        game = self.games[game_id]
        if msg_id != game.msg_id + 1:
            self.send_error(address, "Invalid message ID")
            return

        state = int.from_bytes(state, 'big')
        row = state // 512
        col = state % 8

        if not game.update(row, col):
            self.send_error(address, "Invalid move")
            return

        # Send response
        response_flags, response_state = game.get_status()
        response = struct.pack("!IHB18s", game_id, game.msg_id + 1, response_flags, response_state)
        response = response + b'\0' * (65535 - len(response))  # Pad the message to the maximum length
        self.socket.sendto(response, address)
        game.msg_id += 1

        # Check if the game is over
        if response_flags in [Game.X_WON, Game.O_WON, Game.TIE]:
            print(f"Game {game_id} over - {response_flags}")
            del self.games[game_id]

    def check_timeouts(self):
        now = time.time()
        to_remove = [game_id for game_id, game in self.games.items() if now - game.last_update > 300]
        for game_id in to_remove:
            del self.games[game_id]
            print(f"Closed game {game_id} due to timeout")

    def send_error(self, address, msg):
        data = struct.pack("!IHB18s65531s", 0, 0, 0b100000, b'', msg.encode())
        self.socket.sendto(data, address)

class Game:
    X_WON = 0b00100000000000
    O_WON = 0b00010000000000
    TIE = 0b00001000000000

    def __init__(self):
        self.board = [EMPTY] * 9
        self.msg_id = 1
        self.last_update = time.time()
        self.ended = False

    def update(self, row, col):
        if self.ended:
            return False

        self.last_update = time.time()
        self.board[row * 3 + col] = X_PLAYER if self.msg_id % 2 else O_PLAYER
        winner = self.check_win()
        if winner:
            self.ended = True
        return True

    def get_status(self):
        return self.check_win() or Game.TIE, self.encode_state()

    def encode_state(self):
        state = 0
        for i, cell in enumerate(self.board):
            state |= (cell << (16 - 2 * i))
        return state.to_bytes(18, "big")

    def check_win(self):
        wins = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
        for pattern in wins:
            cells = [self.board[i] for i in pattern]
            if cells == [X_PLAYER, X_PLAYER, X_PLAYER]:
                return Game.X_WON
            if cells == [O_PLAYER, O_PLAYER, O_PLAYER]:
                return Game.O_WON
        if all(self.board):
            return Game.TIE
        return 0

if __name__ == "__main__":
    server = TicTacToeServer(SERVER_IP, SERVER_PORT)
    server.start()
