TCP-CLIENT

**Description**: This is a TCP-based chat client designed to connect to a chat server, send and receive chat messages, and interact with the chat server.

**Usage**:

1. **Prerequisites**: Ensure Python 3 is installed on your system.

2. **Running the Client**: Open your terminal or command prompt and run the client script by providing the server hostname and port as command-line arguments. Replace `com>
python3 client.py compnet.cs.du.edu 7775
Setup: Follow the prompts to configure your chat session:
Enter your username (starting with @).
Enter a list of rooms to monitor (starting with #), separated by spaces.
Chatting: You will be connected to the server and can start chatting:
Enter the target room or user (starting with # or @) to send a message.
Enter your message.
Disconnecting: To disconnect gracefully, simply press Ctrl+C.
Functionality:

Connecting: The client establishes a connection to the server and sends a connect message containing the user's name and the list of rooms to monitor.

Receiving Messages: The client can receive chat message history from the server and display it to the user.

Sending Messages: You can send messages to the server, specifying the target room or user.

Messaging Rooms: The client allows messaging to rooms (specifying a target room) and direct messaging (specifying a user).

Error Handling: Error messages received from the server are displayed to the user.

Graceful Disconnect: To disconnect gracefully, use Ctrl+C.

Troubleshooting:

If you encounter issues with the client, ensure that you have provided the correct server hostname and port. Also, make sure your input adheres to the specified format 