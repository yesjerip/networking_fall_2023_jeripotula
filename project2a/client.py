import socket
import json
import sys

# Function to send a message to the server
def send_message(sock, message):
    try:
        json_message = json.dumps(message)
        sock.sendall(json_message.encode('utf-8'))
    except Exception as e:
        print("Error sending message:", str(e))
        sys.exit(1)

# Function to receive and process messages from the server
def receive_messages(sock):
    try:
        data = sock.recv(4096).decode('utf-8')
        if not data:
            print("Server disconnected.")
            sys.exit(0)
        return json.loads(data)
    except Exception as e:
        print("Error receiving message:", str(e))
        sys.exit(1)

# Function to connect to the chat server
def connect_to_server(server_host, server_port):
    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((server_host, server_port))
        return client_socket
    except Exception as e:
        print("Error connecting to the server:", str(e))
        sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 client.py <server_host> <server_port>")
        sys.exit(1)

    server_host = sys.argv[1]
    server_port = int(sys.argv[2])

    try:
        client_socket = connect_to_server(server_host, server_port)
        user_name = input("Enter your username (starting with @): ")
        rooms = input("Enter a list of rooms to monitor (starting with #), separated by spaces: ").split()

        # Send a connect message to the server
        connect_message = {
            "action": "connect",
            "user_name": user_name,
            "targets": rooms
        }
        send_message(client_socket, connect_message)

        print("Connected to the server. You can start chatting.")

        while True:
            # User input
            target = input("Enter the target room or user (starting with # or @) or 'q' to quit: ")
            if target == 'q':
                break
            message_text = input("Enter your message: ")
            message = {
                "action": "message",
                "user_name": user_name,
                "target": target,
                "message": message_text
            }
            send_message(client_socket, message)

            # Check for messages from the server
            message = receive_messages(client_socket)
            print(f"Message from {message['from']} in {message['target']}: {message['message']}")

    except KeyboardInterrupt:
        # Handle Ctrl+C to gracefully disconnect from the server
        disconnect_message = {
            "action": "disconnect"
        }
        send_message(client_socket, disconnect_message)
        print("Disconnected from the server.")
        client_socket.close()
        sys.exit(0)
