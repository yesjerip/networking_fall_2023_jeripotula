import socket
import struct
import random
import sys

class TicTacToeClient:
    def __init__(self, server_ip, server_port):
        self.server_address = (server_ip, server_port)
        self.client_address = ('localhost', 0)  # Bind to any available port on the client machine
        self.game_id = random.randint(0, 0xFFFFFF)
        self.message_id = random.randint(0, 0xFF)
        self.player_name = None
        self.game_state = 0  # Initialize game state
        self.current_player = None

    def send_message(self, message_id, game_flags, game_state, text_message):
        # Construct and send the UDP message
        game_id_bytes = struct.pack('>I', self.game_id)
        message_id_byte = struct.pack('B', message_id)
        game_flags_bytes = struct.pack('>H', game_flags)
        game_state_bytes = struct.pack('>I', game_state)
        text_message_bytes = text_message.encode('utf-8')

        udp_message = game_id_bytes + message_id_byte + game_flags_bytes + game_state_bytes + text_message_bytes
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.sendto(udp_message, self.server_address)

    def receive_message(self):
        # Receive and parse the UDP message from the server
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.bind(self.client_address)  # Bind to the client's address
            data, addr = s.recvfrom(65535)  # Adjust buffer size as needed

        print(f"Received data: {data}")
        # Parse the received data
        game_id, message_id, game_flags, game_state, text_message = struct.unpack('>IBHIB', data)

        # Handle the received message
        if game_flags & 0b001:  # Check if it's a player move (Bit 0: X player to move, Bit 1: O player to move)
            self.game_state = game_state
            self.current_player = 'X' if game_flags & 0b001 else 'O'
            self.display_board()
            print(f"{self.current_player}'s turn:")
            move = self.get_player_move()
            self.game_state |= 1 << (2 * (move - 1))
            self.send_message(self.message_id, 1, self.game_state, "")

        # Handle game over messages
        if game_flags & 0b100:  # Bit 2: X Player win
            self.display_board()
            print("X wins!")
            self.start_new_game_or_exit()
        elif game_flags & 0b010:  # Bit 3: O Player win
            self.display_board()
            print("O wins!")
            self.start_new_game_or_exit()
        elif game_flags & 0b001:  # Bit 4: Tie
            self.display_board()
            print("It's a tie!")
            self.start_new_game_or_exit()
        elif game_flags & 0b101:  # Bit 5: Error
            print(f"Error: {text_message.decode('utf-8')}")

    def start_new_game_or_exit(self):
        choice = input("Start a new game? (Y/N): ").strip().lower()
        if choice == 'y':
            self.send_message(self.message_id, 0, 0, "New game")
            self.receive_message()  # Receive response from the server for the new game
        else:
            sys.exit()

    def start_game(self):
        # Step 1: Send initial message with player name
        player_name = input("Enter your name: ")
        self.player_name = player_name
        initial_message = f"Player {player_name} has joined the game."
        self.send_message(self.message_id, 0, 0, initial_message)

        # Step 2: Receive response from the server
        self.receive_message()

        # Game loop
        while True:
            # Step 3: Obtain player's move and send it to the server
            self.receive_message()

    def get_player_move(self):
        while True:
            try:
                move = int(input("Enter your move (1-9): "))
                if 1 <= move <= 9:
                    return move
                else:
                    print("Invalid move. Please enter a number between 1 and 9.")
            except ValueError:
                print("Invalid input. Please enter a valid number.")

    def display_board(self):
        board = [' ' for _ in range(9)]
        for i in range(9):
            square = (self.game_state >> (2 * i)) & 0b11
            if square == 0b01:
                board[i] = 'X'
            elif square == 0b10:
                board[i] = 'O'
        print(f" {board[0]} | {board[1]} | {board[2]} ")
        print("---+---+---")
        print(f" {board[3]} | {board[4]} | {board[5]} ")
        print("---+---+---")
        print(f" {board[6]} | {board[7]} | {board[8]} ")

    def main(self):
        try:
            self.start_game()
        except KeyboardInterrupt:
            print("\nGame interrupted. Exiting...")
        except Exception as e:
            print(f"An error occurred: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 client.py <server_ip> <server_port>")
        sys.exit(1)

    server_ip = sys.argv[1]
    server_port = int(sys.argv[2])

    client = TicTacToeClient(server_ip, server_port)
    client.main()
